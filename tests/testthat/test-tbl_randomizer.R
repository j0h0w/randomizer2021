test_that("returns a tibble with 15 rows and 3 columns", {
  expect_equal(nrow(tbl_randomizer()), 15)
  expect_equal(ncol(tbl_randomizer()), 3)
})
