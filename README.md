
<!-- README.md is generated from README.Rmd. Please edit that file -->

# randomizeR2021: Fully-Reproducable Randomisation Lists with Auto-Documentation

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://lifecycle.r-lib.org/articles/stages.html#experimental)
[![pipeline
status](https://gitlab.kks.uni-marburg.de/johow/randomizeR2021/badges/main/pipeline.svg)](https://gitlab.kks.uni-marburg.de/johow/randomizeR2021/-/commits/main)
<!-- badges: end -->

## @Gitlab Pages

Deployment mirrored from <https://j0h0w.gitlab.io/randomizer2021/>:  
![](man/figures/titleslide.png)

| Randomization procedure                   | Constructor function |                                                              Parameters |
|:------------------------------------------|---------------------:|------------------------------------------------------------------------:|
| Complete Randomization (CR)               |           `crPar(N)` |                                                           N sample size |
| Random Allocation Rule (RAR)              |          `rarPar(N)` |                                                           N sample size |
| Permuted Block Randomization (PBR)        |         `pbrPar(bc)` |                                                  bc block constellation |
| Rand. Permuted Block Randomization (RPBR) |     `rpbrPar(N, rb)` |                                  N sample size; rb random block lengths |
| Truncated Binomial Design (TBD)           |         `tbdPar(bc)` |                                                  bc block constellation |
| Rand. Truncated Binomial Design (RTBD)    |     `rtbdPar(N, rb)` |                                  N sample size; rb random block lengths |
| Maximal Procedure (MP)                    |      `mpPar(N, mti)` |                             N sample size; mti max. tolerated imbalance |
| Big Stick Design (BSD)                    |     `bsdPar(N, mti)` |                             N sample size; mti max. tolerated imbalance |
| Efrons Biased Coin Design (EBC)           |       `ebcPar(N, p)` |                                N sample size; p biased coin probability |
| Chen’s Design (CHEN)                      | `chenPar(N, mti, p)` |  N sample size; mti max. tolerated imbalance; p biased coin probability |
| Generalized Biased Coin Design (GBCD)     |    `gbcdPar(N, rho)` |                                       N sample size; rho balance factor |
| Adjustable Biased Coin Design (ABCD)      |      `abcdPar(N, a)` |                                         N sample size; a balance factor |
| Bayesian Biased Coin Design (BBCD)        |      `bbcdPar(N, a)` |                                         N sample size; a balance factor |
| Wei’s Urn Design (UD)                     | `udPar(N, ini, add)` | N sample size; ini initial urn composition; add adjustment in each step |
| Hadamard Randomization (HADA)             |         `hadaPar(N)` |                                                           N sample size |

Table from <https://www.jstatsoft.org/article/view/v085i08>.

### Automating Documentation

``` r
(randomizeR2021::rand_doc_md("rarPar")) # will generate
"Randomization was performed using a ‘Random Allocation Rule’ procedure as 
 described in @rosenberger2015randomization and implemented in the ‘randomizeR’ 
 package, Vers. 2.0.0 [@uschner2018randomizeR]." # along with bib metadata:
# <!-- 
#  W. F. Rosenberger and J. M. Lachin (2002) _Randomization in Clinical Trials_.
#   Wiley. 
# 
# Uschner D, Schindler D, Hilgers R, Heussen N (2018). randomizeR: An R Package 
#   for the Assessment and Implementation of Randomization in Clinical Trials. 
#   _Journal of Statistical Software_, *85*(8), 1-22. doi:
#   10.18637/jss.v085.i08 (URL: https://doi.org/10.18637/jss.v085.i08# 
#
# @article{
# uschner2018randomizeR, title = {{randomizeR}: An {R} Package for 
# the Assessment and Implementation of Randomization # in Clinical Trials} 
# author = {Diane Uschner and David Schindler and Ralf-Dieter Hilgers and 
# Nicole Heussen}journal = {Journal of Statistical Software}, year = {2018}, 
# volume = {85}, number = {8}, pages = {1--22}, doi = {10.18637/jss.v085.i08}}
# 
# @book{rosenberger2015randomization,  title={Randomization in clinical trials: 
# theory and practice},  author={Rosenberger, William F and Lachin, John M},  
# year={2015},  publisher={John Wiley \& Sons}} --->
```

### Rendered Output

Randomization was performed using a ‘Random Allocation Rule’ procedure
as described in [Rosenberger and Lachin (2015)](#refs) and implemented
in the ‘randomizeR’ package, Vers. 2.0.0 ([Uschner \_et al.,
2018](#refs)).

### References

W. F. Rosenberger and J. M. Lachin (2002) *Randomization in Clinical
Trials*. Wiley

Uschner D, Schindler D, Hilgers R, Heussen N (2018). randomizeR: An R
Package for the Assessment and Implementation of Randomization in
Clinical Trials. *Journal of Statistical Software*, *85*(8), 1-22. doi:
[10.18637/jss.v085.i08](https://doi.org/10.18637/jss.v085.i08)
